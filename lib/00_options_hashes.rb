def transmogrify(str, opt_hash=nil)
  if opt_hash
    times = nil
    opt_hash.each do |k, v|
      if k == :times
        times = v
      else
        str = str.send(k) if v
      end
    end
    str = str * times if times
  end
  str
end